**What is this about ?**

- context and inception: Marc Jeanmougin wanted to teach materials regarding open source / FLOSS (he is an expert and well known maintainer of Inkscape, a well known open source project) and replied to the IMT Call for participation for the creation of MOOC to get funding.

- other working on similar courses: Framasoft, OpenStack, Open edX. Xavier Antoviaque from Open edX/OpenCraft was already starting a similar course

- accouncement on forums -> we all started talking and merged our efforts into ONE project.

- since then, all the meetings (brainstorming, creating content, discussing specific topics like business model etc) have been contributed collaboratively by the multiple organizations involved, are completely open to anyone that wants to contribute and all the materials are public and reusable (CC-BY-SA), accessible anytime on a public repository (public website on https://gitlab.com/mooc-floss/mooc-floss/ ) 
 
**How can we generate revenue ?**

The whole production process is completely different from what IMT is used to because of this workflow. The way of handling revenues is different: we need to take into account the multiple producers.

There are new ways of generating revenues and the revenue splitting should be discussed to be fair to all the contributors and to ensure that all producers keep investing on the long term.

- The classic business model applied at IMT is generating finding private funding and getting revenues from **paid certificates** on edX.org. The revenue from paid vertificates is usually splitted this way: 50% edX 50% the producer of the MOOC. To be discussed: how to negociate a better sharing with edX and to the multiple producers ? 

To note: compared to "standard MOOCs" on edX, this specific MOOC is really of interest for edX because it is becoming part of their set of course for onboarding contributors to the open edx platform.

example of a fair splitting: proprotionnaly to the investment (in time/money). 

- This MOOC opens new opportunities for monetization: mentoring, professional training. The goal of the MOOC is to contribute to a FLOSS project. Each FLOSS project has a very specific community, way of working, coding, different contributing guides, technologies used etc... Members of the different communities may come during the MOOC to offer specific **mentoring services** dedicated to their FLOSS project with their specificities. Mentoring services might be free or paid for the students, at the choice of the mentors. Open for discussion: how do we match students to mentors depending on their project's choice within the MOOC ?
- The content of the MOOC might be also used by FLOSS communities or trainers to deliver face-to-face mentorship bootcamps / workshops / **professionnal training** for individuals, groups or companies. Those face-to-face events might be free or paid for the students, at the choice of the FLOSS communities or trainers.

**How do we not generate revenue ?**
Using an open licence (CC-BY-SA or AGPL), was a necessary condition to the involvement of other organizations, who needed the guarantee of a license to be able to keep reusing their own content -- so there can be **no exclusivity on the content distribution**. There is no way to prevent other people to host it or present it on other platforms or paywall the content. We need to rely on the IMT "brand" to be the "reference" platform of the mooc, and to stay active on the maintenance of the MOOC to keep it the default choice of people wanting training on FLOSS. 

- the default edX.org paywall system consists of 1/ a time-limit paywall (the student has to pay to get back the access after a time-limit) 2/ content behind a paywall by default. 










