0
00:00:05,040 --> 00:00:09,880
so say it&#39;s really

1
00:00:06,720 --> 00:00:12,040
hard it is hard I think is when I

2
00:00:09,880 --> 00:00:14,080
started I remember people tell me no

3
00:00:12,040 --> 00:00:17,359
it&#39;s easy to contribute it&#39;s

4
00:00:14,080 --> 00:00:19,720
easy it&#39;s not right this is not the

5
00:00:17,359 --> 00:00:22,920
right I think you you should realize it

6
00:00:19,720 --> 00:00:25,000
is hard as I said can be hostile can be

7
00:00:22,920 --> 00:00:27,080
difficult so don&#39;t fall for that it&#39;s

8
00:00:25,000 --> 00:00:30,480
not easy the thing that you need to have

9
00:00:27,080 --> 00:00:33,040
is patience um and really keep work on

10
00:00:30,480 --> 00:00:35,200
it if it that&#39;s what you want right keep

11
00:00:33,040 --> 00:00:37,040
work on it don&#39;t give up and then look

12
00:00:35,200 --> 00:00:39,960
for the right people because there is

13
00:00:37,040 --> 00:00:39,960
nice people there to

14
00:00:46,199 --> 00:00:49,199
help

