0
00:00:05,200 --> 00:00:09,599
I think what helps is to find the right

1
00:00:07,240 --> 00:00:12,120
people because sadly yes there are very

2
00:00:09,599 --> 00:00:14,679
hostile communities a lot of Open Source

3
00:00:12,120 --> 00:00:17,000
ideas are based on meritocracy the idea

4
00:00:14,679 --> 00:00:19,520
that you just you know everyone has an

5
00:00:17,000 --> 00:00:22,000
equal chance which is not true not

6
00:00:19,520 --> 00:00:25,519
everyone has the time or the computer

7
00:00:22,000 --> 00:00:28,000
resources to work on it so you need to

8
00:00:25,519 --> 00:00:30,880
find the people who understands this and

9
00:00:28,000 --> 00:00:33,520
that can really help you in know for

10
00:00:30,880 --> 00:00:35,920
instance find a mentor you know as I

11
00:00:33,520 --> 00:00:38,640
said go to small um as I did go to

12
00:00:35,920 --> 00:00:41,879
smaller events uh where you feel

13
00:00:38,640 --> 00:00:43,360
comfortable MH you know try to I mean

14
00:00:41,879 --> 00:00:46,600
this is hard of course if you don&#39;t have

15
00:00:43,360 --> 00:00:49,000
a lot of no um communication skills yeah

16
00:00:46,600 --> 00:00:51,199
um but yeah looking for the right

17
00:00:49,000 --> 00:00:53,520
project something that interestes you

18
00:00:51,199 --> 00:00:56,640
and looking for people who are welcoming

19
00:00:53,520 --> 00:00:58,239
and if as I say the point is sadly

20
00:00:56,640 --> 00:01:00,359
sometimes you have to leave if something

21
00:00:58,239 --> 00:01:02,199
H bad happens leave look for another

22
00:01:00,359 --> 00:01:04,360
Community because there is communities

23
00:01:02,199 --> 00:01:06,479
out there like nexcloud cuz I really

24
00:01:04,360 --> 00:01:09,560
always felt safe at nextcloud okay

25
00:01:06,479 --> 00:01:11,080
everyone luckily so far my experience

26
00:01:09,560 --> 00:01:14,040
always been great with my colleagues

27
00:01:11,080 --> 00:01:16,479
with the community people are respectful

28
00:01:14,040 --> 00:01:18,200
we have a code of conduct and that

29
00:01:16,479 --> 00:01:20,400
really followed you know something

30
00:01:18,200 --> 00:01:23,240
happens that person um if someone does

31
00:01:20,400 --> 00:01:26,280
something bad that person gets you know

32
00:01:23,240 --> 00:01:28,240
um uh gets a warning and sometimes even

33
00:01:26,280 --> 00:01:30,520
get you know blocked from GitHub or

34
00:01:28,240 --> 00:01:33,799
something uh depends on the situation

35
00:01:30,520 --> 00:01:35,600
right um but there you need to find as a

36
00:01:33,799 --> 00:01:37,880
someone for minority you should look for

37
00:01:35,600 --> 00:01:41,840
a place that you feel safe that is safe

38
00:01:37,880 --> 00:01:45,920
that has rules that protect you um and

39
00:01:41,840 --> 00:01:47,920
yeah find a mentor if you can um and

40
00:01:45,920 --> 00:01:49,759
maybe look for arest in the project that

41
00:01:47,920 --> 00:01:51,399
you feel comfortable but there is so

42
00:01:49,759 --> 00:01:54,399
many groups out there that are really

43
00:01:51,399 --> 00:01:55,600
focused on being welcoming you know like

44
00:01:54,399 --> 00:01:58,799
I mean so many groups you can just

45
00:01:55,600 --> 00:02:00,520
search for woman in Tech or woman in

46
00:01:58,799 --> 00:02:03,399
open source there&#39;s a project called

47
00:02:00,520 --> 00:02:05,640
open source diversity that I know the

48
00:02:03,399 --> 00:02:08,039
people behind it so like they really

49
00:02:05,640 --> 00:02:10,520
focus on creating this environment where

50
00:02:08,038 --> 00:02:13,400
you can all get together ask questions

51
00:02:10,520 --> 00:02:15,680
feel safe to get started you know and

52
00:02:13,400 --> 00:02:17,879
get this friendly let&#39;s say a friendly

53
00:02:15,680 --> 00:02:20,120
face to help you into you know creating

54
00:02:17,879 --> 00:02:23,720
a first pro request

55
00:02:20,120 --> 00:02:23,720
or however you want to

56
00:02:28,840 --> 00:02:31,840
contribute

