0
00:00:00,080 --> 00:00:06,960
uh this is actually a quite funny story

1
00:00:02,240 --> 00:00:10,960
for me um I was using a floss tool

2
00:00:06,960 --> 00:00:14,360
called inscape a while ago like 10 or

3
00:00:10,960 --> 00:00:18,279
more than 50 years ago and I was using

4
00:00:14,360 --> 00:00:21,519
it for a video game and at some point um

5
00:00:18,279 --> 00:00:23,680
I hit a bug in this program and I didn&#39;t

6
00:00:21,519 --> 00:00:26,519
know much about programming I was just

7
00:00:23,680 --> 00:00:30,119
in my high school or something like that

8
00:00:26,519 --> 00:00:32,239
uh but I I thought that I could report

9
00:00:30,119 --> 00:00:35,800
the bug that because it hit me quite

10
00:00:32,238 --> 00:00:40,239
hard it was a big problem for me and so

11
00:00:35,800 --> 00:00:42,719
I I went to some chats it was IRC but I

12
00:00:40,239 --> 00:00:45,320
didn&#39;t know much at the time and they

13
00:00:42,719 --> 00:00:47,160
reported it and people told me oh but

14
00:00:45,320 --> 00:00:49,520
you here is the source code if you find

15
00:00:47,160 --> 00:00:52,920
where it is you can fix it and at this

16
00:00:49,520 --> 00:00:55,160
point I was okay I I I see like half a

17
00:00:52,920 --> 00:00:57,840
million or a million lines of code and I

18
00:00:55,160 --> 00:01:00,519
have no idea what to do with that and

19
00:00:57,840 --> 00:01:03,320
actually some years later

20
00:01:00,519 --> 00:01:06,479
I tried again to look at the code after

21
00:01:03,320 --> 00:01:08,840
I did some programming I found it quite

22
00:01:06,479 --> 00:01:11,200
easy because uh at first I was just

23
00:01:08,840 --> 00:01:13,159
reporting a b and I stayed on the IRC

24
00:01:11,200 --> 00:01:15,759
channel for a while for for all these

25
00:01:13,159 --> 00:01:18,479
years I actually stayed on this IRC

26
00:01:15,759 --> 00:01:19,920
Channel and I helped others that had

27
00:01:18,479 --> 00:01:21,799
questions that I knew the answer for

28
00:01:19,920 --> 00:01:24,280
because they were trying to find help

29
00:01:21,799 --> 00:01:26,000
using inscape and I could answer those

30
00:01:24,280 --> 00:01:27,439
question I could not help with

31
00:01:26,000 --> 00:01:30,479
development at this time but I could

32
00:01:27,439 --> 00:01:33,720
answer so I integrated myself uh little

33
00:01:30,479 --> 00:01:36,159
by little over the years and when other

34
00:01:33,720 --> 00:01:38,040
people like developers discussed code I

35
00:01:36,159 --> 00:01:39,360
was actually in the channel reading what

36
00:01:38,040 --> 00:01:42,200
they were talking about and

37
00:01:39,360 --> 00:01:44,399
understanding a little so that&#39;s how I R

38
00:01:42,200 --> 00:01:45,920
up my knowledge about the projects it&#39;s

39
00:01:44,399 --> 00:01:49,399
by just staying around on the

40
00:01:45,920 --> 00:01:49,399
communication channels

