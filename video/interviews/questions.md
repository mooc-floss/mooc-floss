Interview Questions
===================

Recap of all questions suggested for the interviews so far:

### Test interview

* How did you get involved into FLOSS? - what was it like?
  * Tell the story of your first contribution / how you began contributing to a project with a community? (or third-party)
  * What did you do exactly? What issues did you face?
  * Did the contribution get accepted? If not, what was different for your first _accepted_ contribution?
  * How did you choose the project? Why did you do that contribution to it? Were you a user of the project before, and if so when did you consider doing more than using the project?
  * How was it to contribute for you? Easy/difficult, why? Is it easier because of who you are, or do you also have to convince people?
  * How did you feel? Sharing the "fear/shyness" to start contributing
* Impostor syndrome - did/do you feel it? ("If I post that publicly, everyone will see that I'm not such a great coder after all. Free software maintainers guys are way better than me.")
* How is it to do work publicly? To have to put yourself forward, in front of everyone?
* Contributing
  * What are other ways than code that you have contributed to open-source projects, or have appreciated seeing others contribute? (documentation, triaging or filing bugs, speaking at conferences...)
  * What advantages do you see in being an open source contributor? What have been the benefits for you personally to be involved in FLOSS, and how do you think it differs from the proprietary world? 
  * What are, in your experience, effective ways to ensure your contributions to a third-party project get accepted? How do you deal with maintainers?
  * What techniques or approaches have you found useful to get started with a project with a large code base that is new to you? Do you have examples of bugfixes that you have contributed in a large third-party project that you didn't know? How did you achieve that, and got it accepted upstream?


### Community

* What do you think about FLOSS communities? About the contributors in the project you have participated in or met?
* What was your most interesting experience with free/libre/open source software?
* How do you think good collaboration happens in software, in general?
* We explain in the course that the most important asset in an open source project is not its technical part, but its community. Would you agree with this point? Could you provide a few examples from your experience that illustrates this well for you?
* There are still some big representation gaps in tech communities (gender and continental in particular), even more so in open source communities. How do you see that evolving in the future? 
* How easy do you think it is to engage with the community of a FLOSS project? What advice would you give our students about it?

### History

* You've been around the world of free/libre/open source software for some time, and could follow its evolution over the years (or decades), could you tell us about it? 

### Philosophy

* What concrete benefits of developing a free/libre/open source project have you observed in your experience? Do you have a few examples? Why were they important? 
* There are some ideological divides about copyleft vs permissive licenses, using non-free tools (like github) vs being more accessible, etc. What would be your main takeaways about these?

### Contributing

* What would you say to new contributors, contributing for the first time to a project?

### Misc

* Of the various terms used to denote Free/Libre/Open source software, which would you like me to use in the interview? Why?
* Twitter: the shitstorm metaphor !
* Would you like to add a last word? 

Note: it might also make sense to adapt the questions to the interviewee: interview one person at a time, prepare a very tailored set of questions based on who answers, and try to iterate with each interview.
